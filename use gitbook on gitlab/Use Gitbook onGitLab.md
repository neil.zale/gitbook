# GitLab 上使用 Gitbook

[註冊一個Gitlab帳號](https://gitlab.com/users/sign_in)

首先到這取得[book範本](https://gitlab.com/pages/gitbook)

![gitbook_tutorial_1.png](assets/gitbook_tutorial_1.png)

GitLab 接著會提示請選擇命名空間，選自己.

接著進到自己剛剛 Fork 的專案中，於左側選擇 設定 > 一般，並展開 Advanced

![gitbook_tutorial_3.png](assets/gitbook_tutorial_3.png)

然後選擇 Remove fork relationship，沒錯 我們要移除與主專案的連結，將這個專案變成自己的

![gitbook_tutorial_4.png](assets/gitbook_tutorial_4.png)

輸入 Project name "gitbook" 解除連結

![gitbook_tutorial_5.png](assets/gitbook_tutorial_5.png)

回到專案首頁，選擇 README.md，隨便編輯一下內容並儲存，會觸發 GitLab 產生頁面，然後可左側的 CI/CD 確認是否已經開始建造

![gitbook_tutorial_6.png](assets/gitbook_tutorial_6.png)

Gitbook on Gitlab 已經設定完成.
之後可以使用GitBook Editor 或用 GitHub Desktop 做檔案編輯及push工具.
我是使用[VS code](https://code.visualstudio.com/) + [Docs markdown](https://code.visualstudio.com/docs/languages/markdown) 套件 ,最後使用[Source tree](https://www.sourcetreeapp.com/)管理.