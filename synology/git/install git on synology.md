# **在Synology 使用Git**

## Server端

1. 在Synology上安裝「Git Server」套件。
1. 建立一個Git Repository的目錄，並將要建立的專案目錄建立在這個目錄下。
1. 在電腦上，透過ssh登入Synology主機(需在「終端機&SNMP」裡開啟SSH登入功能)。進入該專案目錄下後，初始化這個專案的git repo。


### 建立存放Git的位置
![1.png](assets/1.png)

1. 先在「共用資料夾」裡新增一個Git Repository的目錄，假設我們取名「git-repo」
1. 再在這個目錄下建立專案的目錄，假設我們現在要建立的專案名稱是「TEST_PROJECT」
1. 資料夾建立後，要記得修改權限。看你之後要用哪一個使用者登入，該使用者要有這個資料夾的讀寫權限。

在PC上透過SSH登入Synology主機，並cd到該專案的資料夾下，輸入

`git --bare init`

![2.PNG](assets/2.png)

Server端的專案目錄就算建立完成



## Client端

Client端使用TortoiseGit

Client端兩種操作可能，
**1. 第一次要初始化的上傳(上傳最原始的程式碼版本)**


第一種:
在已經有程式碼的資料夾建立Git版本庫
提交這個Git版本庫

在已經有程式碼的資料夾上，點選右鍵，選擇「Git在此建立版本庫」
![3.png](assets/3.png)

不要勾選「設為純版本庫」

![4.png](assets/4.png)

測試新增一個檔案然後右鍵選擇「Git 提交(C) -> “master”

在按下commit & push 後出現push 畫面
選擇"manage"按鈕

輸入:
SSH的語法格式如下：
ssh://username@ip:port/路徑

![7](assets/7.png)

成功push畫面

![8](assets/8.png)


*這裡原本應該會出現要求輸入密碼, 但我已經事前設定好自動使用ssh key*

**2. 是Server上已經有程式碼版本，直接抓取最新的版本下來**

複製遠端的Git版本庫
![9](assets/9.png)


輸入遠端Git Repository的URL與複製到本地端後的位址，再點選「確定」

![10](assets/10.png)


### 上傳SSH key公鑰到git server

如果已經有key可以使用已下指令上傳到synology

`scp C:/Users/[User Name]/.ssh/id_rsa.pub admin@syno_ip11:/volume1/homes/[Nas User]/
`


```markdown
ssh admin@[Syno_Host]
cd /volume1/homes/[Nas User]
mkdir .ssh
cat id_rsa.pub >> .ssh/authorized_keys
chmod 644 .ssh/authorized_keys
chmod 700 .ssh
sudo chown -R [Nas User]:users .ssh
# 需要把user的home目錄設置為755
cd ..
sudo chmod 755 [Nas User]
```
