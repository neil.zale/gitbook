# Server與流量管理—Cacti


`sudo apt-get install cacti`

![1.jpg](../assets/1.jpg)

選擇Apache2

![2.jpg](../assets/2.jpg)

選擇YES配置資料庫

![3.jpg](../assets/3.jpg)

輸入管理者及MySQL密碼

調整php.ini設定

```
sudo vi /etc/php5/apach2/php.ini
max_execution_time = 60
max_input_time = 120
memory_limit = 256M
```
在網址輸入
  http://127.0.0.1/cacti 進行設定

![4.jpg](../assets/4.jpg)

選擇new install

![5.jpg](../assets/5.jpg)

預設Cacti會抓到所有的路徑資訊，點選[Finish]

![6.jpg](../assets/6.jpg)

登入的Cacti網頁，第一次請輸入帳/密:admin/admin

![7.jpg](../assets/7.jpg)

